FROM rust:1.61 as build

RUN USER=root cargo new --bin my_project
WORKDIR /my_project

COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

RUN rustup toolchain install nightly
RUN cat ~/.cargo/config
RUN cargo update
RUN cargo +nightly run -Z no-index-update
RUN cargo build --release
RUN rm src/*.rs

COPY ./src ./src

RUN rm ./target/release/deps/my_project*
RUN cargo build --release

FROM rust:1.61

COPY --from=build /my_project/target/release/my_project .

CMD ["./my_project"]