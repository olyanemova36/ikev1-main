use std::fs::{File};
use std::io::{Write};
use crate::arguments::enums::{EncodeType, HashType};
use constants::*;

mod constants;

pub fn compute_file(hash_t: HashType, enc_t: EncodeType, password: &str) {
    let path = &*get_file_name(hash_t, enc_t, password);

    let mut f = File::create(path).expect("Unable to create file");

    f.write_all(hash_t.id().to_string().as_bytes()).expect("Unable to write data");
    f.write_all(DELIMITER.to_string().as_bytes()).expect("Unable to write data");
    f.write_all(enc_t.id().to_string().as_bytes()).expect("Unable to write data");
    f.write_all(DELIMITER.to_string().as_bytes()).expect("Unable to write data");
    f.write_all(NI.to_string().as_bytes()).expect("Unable to write data");
    f.write_all(DELIMITER.to_string().as_bytes()).expect("Unable to write data");
    f.write_all(NR.to_string().as_bytes()).expect("Unable to write data");
    f.write_all(DELIMITER.to_string().as_bytes()).expect("Unable to write data");
    f.write_all(CI.to_string().as_bytes()).expect("Unable to write data");
    f.write_all(DELIMITER.to_string().as_bytes()).expect("Unable to write data");
    f.write_all(CR.to_string().as_bytes()).expect("Unable to write data");
    f.write_all(DELIMITER.to_string().as_bytes()).expect("Unable to write data");
    f.write_all(EK.to_string().as_bytes()).expect("Unable to write data");
}

fn get_file_name(hash_t: HashType, enc_t: EncodeType, password: &str) -> String {
    let mut path = String::new();

    path.push_str("./");
    path.push_str(password);
    path.push_str(DELIMITER_FILE);
    path.push_str(&hash_t.to_string());
    path.push_str(DELIMITER_FILE);
    path.push_str(&enc_t.to_string());
    path.push_str(".txt");

    return path;
}