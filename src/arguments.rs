extern crate clap;
pub mod enums;

use std::fmt;
use ::clap::{Arg,App,ColorChoice};
use enums::{HashType, EncodeType};

pub struct Arguments {
    pub hash : HashType,
    pub enc : EncodeType,
    pub password : String
}

impl Arguments {
    pub fn new(hash: &str, enc: &str, passw: &str) -> Self {
        let hash_type = match hash {
            "md5" => HashType::MD5,
            "sha1" => HashType::SHA1,
            _ => todo!()
        };

        let enc_type = match enc {
            "aes128" => EncodeType::AES128,
            "aes192" => EncodeType::AES192,
            "aes256" => EncodeType::AES256,
            "3des" => EncodeType::DES3,
            _ => todo!()
        };

        Self {
            hash: hash_type,
            enc: enc_type,
            password: String::from(passw),
        }
    }
}

impl fmt::Display for Arguments {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut result = "hash: ".to_owned();

        result.push_str(&self.hash.to_string());
        result.push_str(" enc: ");
        result.push_str(&self.enc.to_string());
        result.push_str(" password: ");
        result.push_str(&self.password);

        write!(f, "{}", &*result)
    }
}

pub fn params() -> Arguments {
    let matches = App::new("IKEv1 Main Mode")
        .version("0.1.0")
        .author("Nemova Olga <olyanemova36@gamil.com>")
        .about("Creates test files for further active encode attack.")
        .disable_colored_help(true)
        .arg(Arg::with_name("hash")
            .short('h')
            .long("hash")
            .required(true)
            .display_order(1)
            .action(clap::builder::ArgAction::Set)
            .value_parser(clap::builder::PossibleValuesParser::new(["md5", "sha1"]))
            .help("Hash alorithm mode for data encoding."))
        .arg(Arg::with_name("algo")
            .short('a')
            .long("algo")
            .required(true)
            .display_order(2)
            .ignore_case(true)
            .action(clap::builder::ArgAction::Set)
            .value_parser(clap::builder::PossibleValuesParser::new(["3des", "aes128", "aes192", "aes256"]))
            .help("Encoding alorithm mode for full data encoding."))
        .arg(Arg::with_name("password")
            .short('p')
            .long("password")
            .required(true)
            .display_order(3)
            .action(clap::builder::ArgAction::Set)
            .ignore_case(true)
            .help("Custom password for full data encoding."))
        .color(ColorChoice::Auto)
        .get_matches();


    let hash_algo= matches.value_of("hash").unwrap_or_default();
    let enc_algo = matches.value_of("algo").unwrap_or_default();
    let password_arg = matches.value_of("password").unwrap_or_default();

    Arguments::new(hash_algo, enc_algo, password_arg)
}