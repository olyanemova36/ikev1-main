use std::env;
use arguments::params;

mod arguments;
mod file;

#[warn(unused_imports)]
fn main() {
    let args = params();
    file::compute_file(args.hash, args.enc, &*args.password);

    println!("{:?}", args.to_string());
}

#[test]
fn test_correct_output() {
    assert_eq!(0, 0);
}
