use std::fmt;

#[derive(Debug, Clone, Copy)]
pub enum HashType {
    MD5,
    SHA1
}

impl HashType {
    pub fn id(&self) -> i32 {
        match self {
            HashType::MD5 => 1,
            HashType::SHA1 => 2
        }
    }
}

impl fmt::Display for HashType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            HashType::MD5 => write!(f, "md5"),
            HashType::SHA1 => write!(f, "sha1")
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum EncodeType {
    DES3,
    AES128,
    AES192,
    AES256
}

impl EncodeType {
    pub fn id(&self) -> i32 {
        match self {
            EncodeType::DES3 => 5,
            EncodeType::AES128 => 7,
            EncodeType::AES192 => 8,
            EncodeType::AES256 => 9,
        }
    }
}

impl fmt::Display for EncodeType {

    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            EncodeType::DES3 => write!(f, "3des"),
            EncodeType::AES128 => write!(f, "aes128"),
            EncodeType::AES192 => write!(f, "aes192"),
            EncodeType::AES256 => write!(f, "aes256"),
        }
    }
}
